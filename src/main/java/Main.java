import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.mail.SimpleEmail;
import services.email.Conf;
import services.email.EmailService;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());
    public static void main(String[] args) throws EmailException {
        Client client = Client.create();
        WebResource webResource = client.resource("https://jsonplaceholder.typicode.com/posts/");

        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

        if(true) {
            logger.info("Hello");
            logger.debug("Hello1");
            logger.error("HelloError");
        }


        EmailService emailService = new EmailService(new SimpleEmail(), setConfigurationEmail());
        emailService.run();
    }

    private static Conf setConfigurationEmail() {
        Conf conf = new Conf();
        conf.setHostName(Conf.Hosts.MAILTRAP.getHost());
        conf.setPort(Conf.Hosts.MAILTRAP.getPort());
        conf.setUsername("your_username");
        conf.setPassword("your_password");
        conf.setFrom("piwko.patryk1@gmail.com");
        conf.setMessage("exampleMessage2222");
        conf.setTo("emailto@gmail.com");
        return conf;
    }
}
