package services.email;

public class Conf {

    public enum Hosts {
        MAILTRAP("smtp.mailtrap.io");

        private String host;

        Hosts(String hosts) {
            this.host = hosts;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Integer getPort() {
            switch (this.host) {
                case "smtp.mailtrap.io":
                    return 2525;
            }

            return null;
        }
    }


    private Integer port;
    private String hostName;
    private static String username = "3fb29e5def86a2";
    private static String password = "530508db865b9a";

    private String from;
    private String subject;
    private String message;
    private String to;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
