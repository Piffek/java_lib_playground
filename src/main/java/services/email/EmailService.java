package services.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;

public class EmailService {
    private Email email;
    private Conf conf;

    public EmailService(Email email, Conf conf) {
        this.email = email;
        this.conf = conf;
    }

    public void run() throws EmailException {
        email.setHostName(conf.getHostName());
        email.setSmtpPort(conf.getPort());
        email.setAuthenticator(new DefaultAuthenticator(conf.getUsername(), conf.getPassword()));
        email.setFrom(conf.getFrom());
        email.setSubject(conf.getSubject());
        email.setMsg(conf.getMessage());
        email.addTo(conf.getTo());
        email.send();

    }
}
